﻿using System;
using Random = UnityEngine.Random;

namespace Extensions
{
    public static class EnumExtension
    {
        public static T GetRandomValue<T>(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException("Type provided is not an enum.");

            Array values = Enum.GetValues(enumType);
            int randomIndex = Random.Range(1, values.Length);
            
            return (T)values.GetValue(randomIndex);
        }
    }
}