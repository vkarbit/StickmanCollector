using System;
using System.Text;
using UnityEngine;

public static class TimeFormatExtension
{
    private const int WeekSeconds = 60 * 60 * 24 * 7;
    private const int DaySeconds = 60 * 60 * 24;
    private const int HourSeconds = 60 * 60;
    private const int MinuteSeconds = 60;
    private const int MaxCounter = 2;
    private const char SpaceChar = ' ';
    private const char DotsChar = ':';
    private static readonly char ThinSpace = Convert.ToChar(0x202F);

    private static readonly string[] _withNumbers = new string[61];
    private static readonly string[] _withNumbersXX = new string[10];
    private static readonly string[] _timerNames = new string[5];

    private static readonly StringBuilder _stringBuilder = new StringBuilder();
    private static string _zeroTimeString = "0s";
    private static string w, d, h, m, s;


    #region LIVECYCLE METHODS

    static TimeFormatExtension()
    {
        LocalizationHandler();

        for (int i = 0; i < _withNumbers.Length; i++)
        {
            _withNumbers[i] = i.ToString();
        }

        for (int i = 0; i < _withNumbersXX.Length; i++)
        {
            _withNumbersXX[i] = i.ToString("D2");
        }
    }

    #endregion


    public static string FormatTimerFull(float timeInSeconds, bool withLetter = false)
    {
        return Format(timeInSeconds, true, withLetter);
    }

    public static string FormatTimer(float timeInSeconds, bool withLetters = true)
    {
        return FormatDynamicTimer(timeInSeconds, withLetters, 2);
    }

    public static string Format(float timeInSeconds, bool isTimer = false, bool withLetters = false, bool withWeeks = true,
        bool withDays = true)
    {
        if (timeInSeconds < 0 || float.IsNaN(timeInSeconds))
        {
            return _zeroTimeString;
        }

        int nWeeks = 0;
        int nDays = 0;
        int nHours = 0;
        int nMinutes = 0;

        if (withWeeks)
        {
            CalculateTime(WeekSeconds, ref nWeeks, ref timeInSeconds);
        }

        if (withDays)
        {
            CalculateTime(DaySeconds, ref nDays, ref timeInSeconds);
        }

        CalculateTime(HourSeconds, ref nHours, ref timeInSeconds);
        CalculateTime(MinuteSeconds, ref nMinutes, ref timeInSeconds);

        int nSeconds = (int)timeInSeconds;

        if (nWeeks == 0 && nDays == 0 && nHours == 0 && nMinutes == 0)
        {
            nSeconds = Mathf.FloorToInt(timeInSeconds);
        }

        string wString = (nWeeks >= _withNumbers.Length) ? nWeeks.ToString() : _withNumbers[nWeeks];
        string dString = (nDays >= _withNumbers.Length) ? nDays.ToString() : _withNumbers[nDays];
        string hString = (nHours >= _withNumbers.Length) ? nHours.ToString() : _withNumbers[nHours];
        string mString = (nMinutes < 10 && nHours > 0) ? _withNumbersXX[nMinutes] : _withNumbers[nMinutes];
        string sString = (nSeconds < 10 && nMinutes > 0) ? _withNumbersXX[nSeconds] : _withNumbers[nSeconds];

        int counter = 0;

        _stringBuilder.Length = 0;

        if (nWeeks != 0 && withWeeks)
        {
            FormatTimeCategory(withLetters, wString, w, ref counter);
            if (counter == MaxCounter) return _stringBuilder.ToString();
        }

        if (nDays != 0 && withDays)
        {
            FormatTimeCategory(withLetters, dString, d, ref counter);
            if (counter == MaxCounter) return _stringBuilder.ToString();
        }

        if (isTimer && nHours != 0)
        {
            FormatTimeCategory(withLetters, hString, h, ref counter);
            if (counter == MaxCounter) return _stringBuilder.ToString();
        }

        if (isTimer && nMinutes != 0)
        {
            FormatTimeCategory(withLetters, mString, m, ref counter);
            if (counter == MaxCounter) return _stringBuilder.ToString();
        }

        if (isTimer || _stringBuilder.Length == 0)
        {
            FormatTimeCategory(withLetters, sString, s, ref counter);
            if (counter == MaxCounter) return _stringBuilder.ToString();
        }

        return _stringBuilder.ToString();
    }

    private static void CalculateTime(int timeCategoryRange, ref int categoryValue, ref float timeInSeconds)
    {
        int denominator = timeCategoryRange;
        double nRemainder = timeInSeconds / denominator;
        categoryValue = (int)nRemainder;
        timeInSeconds -= (categoryValue * denominator);
    }

    private static void FormatTimeCategory(bool useLetters, string value, string category, ref int counter)
    {
        if (_stringBuilder.Length != 0)
        {
            _stringBuilder.Append(useLetters ? ThinSpace : DotsChar);
        }

        _stringBuilder.Append(value);

        if (useLetters)
            _stringBuilder.Append(category);
        counter++;
    }

    public static string FormatDynamicTimer(float timeInSeconds, bool withLetters = true, int maxCounter = 3,
        int minCounter = 2)
    {
        if (timeInSeconds < 0 || float.IsNaN(timeInSeconds)) return _zeroTimeString;

        int time = Mathf.FloorToInt(timeInSeconds);

        int denominator = 60 * 60 * 24 * 7;

        double nRemainder = timeInSeconds / denominator;
        int nWeeks = (int)nRemainder;
        timeInSeconds -= (nWeeks * denominator);

        denominator = 60 * 60 * 24;

        nRemainder = timeInSeconds / denominator;
        int nDays = (int)nRemainder;
        timeInSeconds -= (nDays * denominator);

        denominator = 60 * 60;

        nRemainder = timeInSeconds / denominator;
        int nHours = (int)nRemainder;
        timeInSeconds -= (nHours * denominator);

        denominator = 60;

        nRemainder = timeInSeconds / denominator;
        int nMinutes = (int)nRemainder;
        timeInSeconds -= nMinutes * denominator;

        int nSeconds = (int)timeInSeconds;

        if (nWeeks == 0 && nDays == 0 && nHours == 0 && nMinutes == 0)
        {
            nSeconds = Mathf.FloorToInt(timeInSeconds);
        }

        int[] timers = new int[5];

        timers[0] = nWeeks;
        timers[1] = nDays;
        timers[2] = nHours;
        timers[3] = nMinutes;
        timers[4] = nSeconds;

        int counter = 0;
        _stringBuilder.Length = 0;

        for (var i = 0; i < timers.Length; i++)
        {
            if ((timers[i] == 0) && (counter == 0) && (i != timers.Length - 1) && (timers.Length - i > minCounter))
            {
                continue;
            }

            var moreThanMinute = i != 3 || time > 59;
            var moreThanOneMinute = time > 60;
            var numberArray = ((counter > 0 || !withLetters) && timers[i] < 10 && moreThanOneMinute) ? _withNumbersXX : _withNumbers;

            if (counter > 0)
            {
                if (moreThanOneMinute)
                {
                    _stringBuilder.Append(DotsChar);
                }
            }

            if (timers[i] >= numberArray.Length)
            {
                _stringBuilder.Append(timers[i]);
            }
            else
            {
                if (moreThanMinute)
                {
                    _stringBuilder.Append(numberArray[timers[i]]);
                }
            }

            if (moreThanMinute)
            {
                if (withLetters)
                {
                    _stringBuilder.Append(_timerNames[i]);
                }
            }

            counter++;

            if (counter >= maxCounter)
            {
                break;
            }
        }

        return _stringBuilder.ToString();
    }

    /// <summary>Возвращает дату, когда было совершено действие в переданном формате</summary>
    /// <param name="time">количество секунд с начала события</param>
    /// <param name="format">формат котором нужно отобразить время</param>
    public static string DateFormatFromNow(double time)
    {
        DateTime date = DateTime.Now.AddSeconds(-time);
        return date.ToString("dd/MM/yyyy");
    }

    /// <summary>Возвращает дату, когда было совершено действие в переданном формате</summary>
    /// <param name="time">количество секунд с начала события</param>
    /// <param name="format">формат котором нужно отобразить время</param>
    public static string TimeFormatFromNow(double time)
    {
        DateTime date = DateTime.Now.AddSeconds(-time);
        return date.ToString("HH:mm");
    }

    /// <summary>Возвращает дату, когда было совершено действие в переданном формате</summary>
    /// <param name="date">DateTime ответа от севера</param>
    /// <param name="time">количество секунд с начала события</param>
    /// <param name="format">формат котором нужно отобразить время</param>
    public static string DateFormatFrom(DateTime date, double time)
    {
        return date.AddSeconds(-time).ToString("dd/MM/yyyy");
    }

    public static string TimeFormatFrom(DateTime date, double time)
    {
        return date.AddSeconds(-time).ToString("HH:mm");
    }

    public static string DateTimeFormatFrom(DateTime date, double time)
    {
        return date.AddSeconds(-time).ToString("dd/MM/yyyy HH:mm");
    }

    private static void LocalizationHandler()
    {
        w = "TIMER_WEEK";
        d = "TIMER_DAY";
        h = "TIMER_HOUR";
        m = "TIMER_MINUTE";
        s = "TIMER_SECOND";
        _zeroTimeString = "0" + s;

        _timerNames[0] = w;
        _timerNames[1] = d;
        _timerNames[2] = h;
        _timerNames[3] = m;
        _timerNames[4] = s;
    }

    public static string FormatWithThinSpace(float val)
    {
        if (val.Equals(0))
        {
            return "0";
        }

        return string.Format("{0:### ### ###} ", val).Trim().Replace(SpaceChar, ThinSpace);
    }
}
