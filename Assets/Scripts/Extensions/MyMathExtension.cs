﻿using System;
using UnityEngine;

namespace Extensions
{
    public static class MyMathExtension
    {
        public static bool EqualTo(this float num1, float num2, float tolerance = 0.01f) => 
            Math.Abs(Math.Round(num1, 2) - Math.Round(num2, 2)) < tolerance;

        public static bool EqualToXZ(this Vector3 vector1, Vector3 vector2, float tolerance = 0.01f) =>
            vector1.x.EqualTo(vector2.x, tolerance) 
            && vector1.z.EqualTo(vector2.z, tolerance);
    }
}