using System;
using UnityEngine.SceneManagement;

public class LoadScene : Process
{
    private readonly SceneName _scene;
    private readonly Action<SceneName> _callback;

    public LoadScene(SceneName scene, Action<SceneName> callback)
    {
        _scene = scene;
        _callback = callback;
    }

    protected override bool OnRun()
    {
        SceneManager.sceneLoaded += Loaded;
        SceneManager.LoadSceneAsync(_scene.ToString(), LoadSceneMode.Additive);
        
        return false;
    }

    private void Loaded(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= Loaded;
        SceneManager.SetActiveScene(scene);
        _callback?.Invoke(_scene);
        
        Complete();
    }
}