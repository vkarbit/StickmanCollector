using UnityEngine.SceneManagement;

public class UnloadCurrentScene : Process
{
    protected override bool OnRun()
    {
        if (SceneManager.GetSceneByName(Services.Scene.Current.ToString()).isLoaded)
        {
            SceneManager.sceneUnloaded += CurrentSceneUnloaded;                 
            SceneManager.UnloadSceneAsync(Services.Scene.Current.ToString());
            return false;
        }
        
        return true;
    }
    
    private void CurrentSceneUnloaded(Scene scene)
    {
        SceneManager.sceneUnloaded -= CurrentSceneUnloaded;  
            
        Complete();
    }
}