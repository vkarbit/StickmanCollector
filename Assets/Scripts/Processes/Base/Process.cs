using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Base;

public abstract class Process : ServicesInjector
{
    public Process()
    {
    }

    public Process(Action<bool> complete)
    {
        _completeWithResult += complete;
    }

    public Process(Action complete)
    {
        _complete += complete;
    }

    private event Action _complete;
    private event Action<bool> _completeWithResult;
    private event Action<Process> _completeWithParent;
    private event Action _forceComplete;

    public bool IsComplete => _completed;
    
    private bool _completed;
    private readonly Queue<Process> _subProcesses = new Queue<Process>();

    public Process Run(Services services)
    {
        _completed = false;
        Inject(services);

        if (OnRun())
            Complete();

        return this;
    }

    protected abstract bool OnRun();

    protected void Add(Process process)
    {
        _subProcesses.Enqueue(process.OnComplete(TryCompleteAll));
        
        OnAdd(process);
    }

    protected virtual void OnAdd(Process process)
    {
        
    }

    public Process OnComplete(Action callback)
    {
        _complete += callback;

        return this;
    }

    public Process OnComplete(Action<bool> callback)
    {
        _completeWithResult += callback;

        return this;
    }

    public Process OnComplete(Action<Process> callback)
    {
        _completeWithParent += callback;

        return this;
    }

    public Process OnForceComplete(Action callback)
    {
        _forceComplete += callback;

        return this;
    }

    public void ForceComplete()
    {
        ClearProcesses();
        CompleteMain(false);
        
        _forceComplete?.Invoke();
        _forceComplete = null;
    }

    protected void Complete()
    {
        CompleteMain(true);
    }

    private void CompleteMain(bool success)
    {
        OnComplete();
        TryCompleteAll(success);
    }

    protected virtual void OnComplete()
    {
    }

    private void TryCompleteAll(bool success)
    {
        if (_subProcesses.Count > 0)
        {
            Services.Processes.Run(_subProcesses.Dequeue());
            return;
        }

        _completed = true;

        _completeWithResult?.Invoke(success);
        _completeWithResult = null;
        _complete?.Invoke();
        _complete = null;

        OnCompleteAll();
        
        _completeWithParent?.Invoke(this);
        _completeWithParent = null;
    }
    
    protected virtual void OnCompleteAll()
    {
    }

    private void ClearProcesses()
    {
        while (_subProcesses.Count > 0)
        {
            _subProcesses.Dequeue().ForceComplete();
        }
    }
}