using Base;

namespace Processes.SplashScreen
{
    public class HideSplashScreen : Process
    {
        private readonly SplashScreenHandler _handler;

        public HideSplashScreen(SplashScreenHandler handler)
        {
            _handler = handler;
        }

        protected override bool OnRun()
        {
            _handler.Close(Complete);
            
            return false;
        }
    }
}