using Base;

namespace Processes.SplashScreen
{
    public class ShowSplashScreen : Process 
    {
        private readonly SplashScreenName _name;
        private readonly SplashScreenHandler _handler;

        public ShowSplashScreen(SplashScreenName name, SplashScreenHandler handler)
        {
            _name = name;
            _handler = handler;
        }

        protected override bool OnRun()
        {
            _handler.Open(_name, Complete);
            
            return false;
        }
    }
}