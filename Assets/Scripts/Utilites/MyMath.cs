﻿namespace Context.Services.InputService
{
    public static class MyMath
    {
        public static float FindProportion(float x1, float x2, float y2) => 
            x1 * y2 / x2;
    }
}