﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Base
{
    [RequireComponent(typeof(Button))]
    public class ChangeButtonSpriteComponent : MonoBehaviour
    {
        [SerializeField] private GameObject _activeObj;
        [SerializeField] private GameObject _deactivateObj;
        [SerializeField] private bool _isChangeOnClick = true;

        private Button _myButton;

        private void Start()
        {
            _myButton = transform.GetComponent<Button>();
            _myButton.onClick.AddListener(OnClickButton);
        }

        public void Initialize(bool active)
        {
            _activeObj.SetActive(active);
            _deactivateObj.SetActive(!active);
        }

        private void OnClickButton()
        {
            if(_isChangeOnClick == false) return;

            _activeObj.SetActive(_deactivateObj.activeInHierarchy);
            _deactivateObj.SetActive(!_activeObj.activeInHierarchy);
        }

        public void SetAvailiable(bool active)
        {
            _activeObj.SetActive(active);
            _deactivateObj.SetActive(!active);
        }

    }
}