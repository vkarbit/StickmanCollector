using DG.Tweening;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class SplashScreenView : BaseView
    {
        [SerializeField] private CanvasGroup _group;
        [SerializeField] private float _fadeInTime = .25f;
        [SerializeField] private float _fadeOutTime = .25f;

        protected override void OnShowStart()
        {
            DOVirtual.Float(_group.alpha, 1, _fadeInTime, UpdateGroupAlpha).OnComplete(OnShowEnd);
        }

        private void UpdateGroupAlpha(float value)
        {
            _group.alpha = value;
        }

        protected override void OnStartHide()
        {
            DOVirtual.Float(1, 0, _fadeOutTime, UpdateGroupAlpha).OnComplete(() => gameObject.SetActive(false));
        }
    }
}