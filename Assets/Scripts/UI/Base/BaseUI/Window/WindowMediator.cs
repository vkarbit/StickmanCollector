﻿
using Base;

namespace UI
{
    public abstract class WindowMediator : BaseMediator
    {
        public WindowName Name { get; private set; }
        
        public void SetName(WindowName window)
        {
            Name = window;
        }

        protected virtual void Close()
        {
            Services.Windows.Close(Name);
        }
    }
    
    public abstract class WindowMediator<T, Z> : WindowMediator where T : WindowView where Z : WindowData
    {
        protected T View { get; private set; }

        protected Z Data { get; private set; }

        protected sealed override void OnSetView(BaseView view)
        {
            View = view as T;
        }

        protected sealed override void OnSetData(BaseData data)
        {
            Data = data as Z;
        }

        protected sealed override void Mediate()
        {
            View.CloseClick += OnCloseClick;
            
            OnMediate();
        }

        protected virtual void OnMediate() { } 

        public sealed override void Show()
        {
            View.Show();
            OnShow();
        }

        protected virtual void OnShow() { }

        public sealed override void Hide()
        {
            if (View != null) View.Hide();
            OnHide();
            _hided?.Invoke();
        }
        
        protected virtual void OnHide() { }

        public sealed override void UnMediate()
        {
            View.CloseClick -= OnCloseClick;
            
            OnUnMediate();
        }

        protected virtual void OnCloseClick()
        {
            Close();
        }

        protected virtual void OnUnMediate()
        {
            
        }
    }
}
