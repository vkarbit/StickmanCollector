using System.Collections.Generic;
using UnityEngine;

public class Mapper<K,V>
{
    private readonly Dictionary<K,V> _maps = new Dictionary<K,V>();
    private readonly bool _useDefaultValue;
    private readonly V _defaultValue;

    public Mapper()
    {
        _useDefaultValue = false;
    }
    
    public Mapper(V defaultValue)
    {
        _useDefaultValue = true;
        _defaultValue = defaultValue;
    }
    
    public void Map(K key, V value)
    {
        if (_maps.ContainsKey(key))
        {
            _maps[key] = value;
            return;
        }
        
        _maps.Add(key, value);
    }

    public bool Contains(K key)
    {
        return _maps.ContainsKey(key);
    }

    public V GetValue(K key)
    {
        V value;
        if (key != null && _maps.TryGetValue(key, out value))
        {
            return value;
        }
            
        return DefaultValue(key);
    }

    public V this[K key]
    {
        get
        {
            V value;
            if (key != null && _maps.TryGetValue(key, out value))
            {
                return value;
            }
            
            return DefaultValue(key);
        }
    }

    private V DefaultValue(K key)
    {
        if (_useDefaultValue)
        {
            return _defaultValue;
        }
        
        Debug.LogError($"Mapper doesn't contains {key} key");
        return default(V);
    }

    public void Dispose()
    {
        _maps.Clear();
    }
}