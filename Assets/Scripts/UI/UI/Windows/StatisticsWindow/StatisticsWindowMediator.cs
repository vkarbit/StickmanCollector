using UI;

public class StatisticsWindowMediator : WindowMediator<StatisticsWindowView, WindowData>
{
    protected override void OnShow()
    {
        base.OnShow();

        View.AppleCollectedTextCount.text = Services.Challenges.ChallengesData.AppleCollected.ToString();
        View.StrawberryCollectedTextCount.text = Services.Challenges.ChallengesData.StrawberryCollected.ToString();
        View.WatermelonCollectedTextCount.text = Services.Challenges.ChallengesData.WatermelonCollected.ToString();
    }
}
