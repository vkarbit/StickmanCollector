using UI;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsWindowView : WindowView
{
    [SerializeField] private Text _appleCollectedTextCount;
    [SerializeField] private Text _strawberryCollectedTextCount;
    [SerializeField] private Text _watermelonCollectedTextCount;

    public Text AppleCollectedTextCount => _appleCollectedTextCount;
    public Text StrawberryCollectedTextCount => _strawberryCollectedTextCount;
    public Text WatermelonCollectedTextCount => _watermelonCollectedTextCount;
}
