using UI;

public class WinWindowMadiator : WindowMediator<WinWindowView, WindowData>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.RestartButton.onClick.AddListener(OnRestartButtonClick);
        View.ReturnBackButton.onClick.AddListener(OnReturnBackButtonClick);
    }

    protected override void OnUnMediate()
    {
        base.OnUnMediate();

        View.RestartButton.onClick.RemoveListener(OnRestartButtonClick);
        View.ReturnBackButton.onClick.RemoveListener(OnReturnBackButtonClick);
    }

    private void OnRestartButtonClick()
    {
        Services.Windows.Close(WindowName.WinWindow);
        Services.Game.OnRestart?.Invoke();
    }
    private void OnReturnBackButtonClick()
    {
        Services.Windows.Close(WindowName.WinWindow);
        Services.GameState.RunTo(Base.GameState.Menu);
        Services.Game.IsSpawn = true;
    }
}
