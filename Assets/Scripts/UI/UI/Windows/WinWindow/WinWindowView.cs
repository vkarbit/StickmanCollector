using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class WinWindowView : WindowView
{
    [SerializeField] private Button _returnBackButton;
    [SerializeField] private Button _restartButton;
    public Button ReturnBackButton => _returnBackButton;
    public Button RestartButton => _restartButton;
}
