using UI;

public class LoseWindowMediator : WindowMediator<LoseWindowView, WindowData>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.ReturnBackButton.onClick.AddListener(OnReturnBackButtonClick);
    }

    protected override void OnUnMediate()
    {
        base.OnUnMediate();
        View.ReturnBackButton.onClick.RemoveListener(OnReturnBackButtonClick);
    }

    private void OnReturnBackButtonClick()
    {
        Services.Windows.Close(WindowName.LoseWindow);
        Services.GameState.RunTo(Base.GameState.Menu);
    }
}
