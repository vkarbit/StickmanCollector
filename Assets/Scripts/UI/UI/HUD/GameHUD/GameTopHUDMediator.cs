using Base;
using DG.Tweening;
using UI;
using UnityEngine;

public class GameTopHUDMediator : HudMediator<GameTopHUDView>
{
    protected override void OnShow()
    {
        base.OnShow();

        GenerateAndShowChallenge();

        Services.Game.OnRestart += GenerateAndShowChallenge;
        Services.Challenges.OnAddNewItem += UpdateCurrentBasketCapacity;
    }

    protected override void OnHide()
    {
        base.OnHide();

        Services.Challenges.CurrentChallenge.OnCollectedNeedCount -= Services.Challenges.OnChallengeCompleted;
        Services.Challenges.CurrentChallenge.OnCurrentValueChanged -= UpdateSliderValue;
        Services.Challenges.CurrentChallenge.OnCurrentValueUpdate -= Services.Challenges.ChallengesData.ChallengeValueUpdate;

        Services.Game.OnRestart -= GenerateAndShowChallenge;
        Services.Challenges.OnAddNewItem -= UpdateCurrentBasketCapacity;
    }

    private void UpdateSliderValue(int value)
    {
        View.CurrentProgressText.text = value.ToString();
        View.ProgeressSlider.DOValue(value, 0.5f);
    }

    private void GenerateAndShowChallenge()
    {
        Services.Challenges.ResetCurrentBasketCapasity();

        var randomChallenge = Services.Challenges.GenerateRandomChallenge();

        Services.Challenges.SetCurrentChallenge(randomChallenge);

        DOVirtual.DelayedCall(0.5f, () => Services.Challenges.CurrentChallenge.OnCollectedNeedCount += Services.Challenges.OnChallengeCompleted);
        Services.Challenges.CurrentChallenge.OnCurrentValueChanged += UpdateSliderValue;
        Services.Challenges.CurrentChallenge.OnCurrentValueUpdate += Services.Challenges.ChallengesData.ChallengeValueUpdate;

        View.ChallengeNameText.text = randomChallenge.Name;
        View.ProgeressSlider.maxValue = randomChallenge.NeedCount;
        View.ProgeressSlider.value = 0;

        View.CurrentProgressText.text = 0.ToString();
        View.NecessatyProgressText.text = randomChallenge.NeedCount.ToString();

        View.CurrentBasketCapacity.text = 0.ToString();
        DOVirtual.DelayedCall(0.2f, () => View.MaxBasketCapacity.text = Services.Challenges.MaxBasketCapasity.ToString());
    }

    private void UpdateCurrentBasketCapacity(int value)
    {
        View.CurrentBasketCapacity.text = value.ToString();
    }

}
