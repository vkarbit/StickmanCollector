using UI;
using UnityEngine;
using UnityEngine.UI;

public class GameTopHUDView : HudView
{
    [SerializeField] private Slider _progeressSlider;
    [SerializeField] private Text _challengeNameText;
    [SerializeField] private Text _currentProgressText;
    [SerializeField] private Text _necessatyProgressText;
    [SerializeField] private Text _currentBasketCapacity;
    [SerializeField] private Text _maxBasketCapacity;

    public Slider ProgeressSlider => _progeressSlider;
    public Text ChallengeNameText => _challengeNameText;
    public Text CurrentProgressText => _currentProgressText;
    public Text NecessatyProgressText => _necessatyProgressText;
    public Text CurrentBasketCapacity => _currentBasketCapacity;
    public Text MaxBasketCapacity => _maxBasketCapacity;
}
