using UI;
using Base;

public class MenuHUDMediator : HudMediator<MenuHUDView>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.PlayButton.onClick.AddListener(OnPlayButtonClick);
        View.StatisticsButton.onClick.AddListener(OnStatisticsButtonClick);
    }
    protected override void OnUnMediate()
    {
        base.OnUnMediate();

        View.PlayButton.onClick.RemoveListener(OnPlayButtonClick);
        View.StatisticsButton.onClick.RemoveListener(OnStatisticsButtonClick);
    }

    private void OnPlayButtonClick()
    {
        Services.GameState.RunTo(GameState.Game);
    }
    private void OnStatisticsButtonClick()
    {
        Services.Windows.Open(WindowName.StatisticsWindow);
    }
}
