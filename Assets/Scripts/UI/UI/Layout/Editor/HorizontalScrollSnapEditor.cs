﻿
using UnityEditor;
using UnityEditor.UI;
using UnityEngine.UI.Extensions;

namespace UnityEngine.UI.Extensions
{
	[CustomEditor(typeof(HorizontalScrollSnap), true)]
	public class HorizontalScrollSnapEditor : ScrollRectEditor
	{
		private SerializedObject _object;

		protected override void OnEnable() {
			base.OnEnable();
			_object = new SerializedObject(target);
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector ();
		}
	}
}


