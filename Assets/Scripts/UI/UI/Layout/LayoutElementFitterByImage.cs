using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SOrca.Flibustiers.Client.Assets.Imported_Systems.Layout
{
  [ExecuteInEditMode]
  [RequireComponent(typeof(LayoutElement), typeof(Image))]
  public class LayoutElementFitterByImage : UIBehaviour
  {
    private enum Maindimension { Width, Height }

    [SerializeField]
#pragma warning disable 649
    private Maindimension mainDimension;
#pragma warning restore 649
    [SerializeField]
    private float mainSize;

    private RectTransform _rectTransform;
    private LayoutElement _layout;
    private Image _image;


    protected override void Awake()
    {
      if (_layout == null) _layout = GetComponent<LayoutElement>();
      if (_image == null) _image = GetComponent<Image>();
      if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
      UpdateSize();
    }

    public void UpdateSize()
    {
      if (_rectTransform == null || _layout == null) return;

      switch (mainDimension)
      {
        case Maindimension.Height:
          _layout.preferredWidth = _rectTransform.rect.height * _image.sprite.AspectRatio();
          break;
        case Maindimension.Width:
          _layout.preferredHeight = _rectTransform.rect.width / _image.sprite.AspectRatio();
          break;
      }
    }
  }

  public static class SpriteExtensions
  {
    public static float AspectRatio(this Sprite sprite)
    {
      return sprite.bounds.size.x / sprite.bounds.size.y;
    }
  }
}