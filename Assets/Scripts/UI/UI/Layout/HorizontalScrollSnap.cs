/// Credit BinaryX 
/// Sourced from - http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/page-2#post-1945602
/// Updated by ddreaper - removed dependency on a custom ScrollRect script. Now implements drag interfaces and standard Scroll Rect.

using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
  [AddComponentMenu("Layout/Extensions/Horizontal Scroll Snap")]
  public class HorizontalScrollSnap : ScrollSnapBase, IEndDragHandler
  {
    new void Start()
    {
      _isVertical = false;
      _childAnchorPoint = new Vector2(0, 0.5f);
      _currentPage = StartingScreen;
      UpdateLayout();
    }

    void Update()
    {
      if (!_lerp && velocity == Vector2.zero)
      {
        if (!_settled && !_pointerDown)
        {
          if (!IsRectSettledOnaPage(content.localPosition))
          {
            ScrollToClosestElement();
          }
        }
        return;
      }

      if (_lerp)
      {
        content.localPosition = Vector3.Lerp(content.localPosition, _lerp_target, transitionSpeed * Time.deltaTime);
        if (Vector3.Distance(content.localPosition, _lerp_target) < 0.1f)
        {
          content.localPosition = _lerp_target;
          _lerp = false;
          EndScreenChange();
        }
        SetCurrentPage(GetPageforPosition(content.localPosition));
      }


      //If the container is moving check if it needs to settle on a page
      if (!_pointerDown)
      {
        if (velocity.x > 0.01 || velocity.x < 0.01)
        {
          //if the pointer is released and is moving slower than the threshold, then just land on a page
          if (IsRectMovingSlowerThanThreshold(0))
          {
            ScrollToClosestElement();
          }
        }
      }
    }

    private bool IsRectMovingSlowerThanThreshold(float startingSpeed)
    {
      return (velocity.x > startingSpeed && velocity.x < SwipeVelocityThreshold) ||
                          (velocity.x < startingSpeed && velocity.x > -SwipeVelocityThreshold);
    }

    private void DistributePages()
    {
      _screens = content.childCount;
      horizontalNormalizedPosition = 0;

      int _offset = 0;
      float _dimension = 0;
      float currentXPosition = 0;
      var pageStepValue = _childSize = (int)panelDimensions.width * ((PageStep == 0) ? 3 : PageStep);


      for (int i = 0; i < content.transform.childCount; i++)
      {
        RectTransform child = content.transform.GetChild(i).gameObject.GetComponent<RectTransform>();
        currentXPosition = _offset + (int)(i * pageStepValue);
        child.sizeDelta = new Vector2(panelDimensions.width, panelDimensions.height);
        child.anchoredPosition = new Vector2(currentXPosition, 0f);
        child.anchorMin = child.anchorMax = child.pivot = _childAnchorPoint;
      }

      _dimension = currentXPosition + _offset * -1;

      content.offsetMax = new Vector2(_dimension, 0f);
    }

    /// <summary>
    /// Add a new child to this Scroll Snap and recalculate it's children
    /// </summary>
    /// <param name="GO">GameObject to add to the ScrollSnap</param>
    public void AddChild(GameObject GO)
    {
      AddChild(GO, false);
    }

    /// <summary>
    /// Add a new child to this Scroll Snap and recalculate it's children
    /// </summary>
    /// <param name="GO">GameObject to add to the ScrollSnap</param>
    /// <param name="WorldPositionStays">Should the world position be updated to it's parent transform?</param>
    public void AddChild(GameObject GO, bool WorldPositionStays)
    {
      horizontalNormalizedPosition = 0;
      GO.transform.SetParent(content, WorldPositionStays);
      InitialiseChildObjectsFromScene();
      DistributePages();
      if (MaskArea) UpdateVisible();

      SetScrollContainerPosition();
    }

    /// <summary>
    /// Remove a new child to this Scroll Snap and recalculate it's children 
    /// *Note, this is an index address (0-x)
    /// </summary>
    /// <param name="index">Index element of child to remove</param>
    /// <param name="ChildRemoved">Resulting removed GO</param>
    public void RemoveChild(int index, out GameObject ChildRemoved)
    {
      RemoveChild(index, false, out ChildRemoved);
    }

    /// <summary>
    /// Remove a new child to this Scroll Snap and recalculate it's children 
    /// *Note, this is an index address (0-x)
    /// </summary>
    /// <param name="index">Index element of child to remove</param>
    /// <param name="WorldPositionStays">If true, the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before</param>
    /// <param name="ChildRemoved">Resulting removed GO</param>
    public void RemoveChild(int index, bool WorldPositionStays, out GameObject ChildRemoved)
    {
      ChildRemoved = null;
      if (index < 0 || index > content.childCount)
      {
        return;
      }
      horizontalNormalizedPosition = 0;

      Transform child = content.transform.GetChild(index);
      child.SetParent(null, WorldPositionStays);
      ChildRemoved = child.gameObject;
      InitialiseChildObjectsFromScene();
      DistributePages();
      if (MaskArea) UpdateVisible();

      if (_currentPage > _screens - 1)
      {
        SetCurrentPage(_screens - 1);
      }

      SetScrollContainerPosition();
    }

    /// <summary>
    /// Remove all children from this ScrollSnap
    /// </summary>
    /// <param name="ChildrenRemoved">Array of child GO's removed</param>
    public void RemoveAllChildren(out GameObject[] ChildrenRemoved)
    {
      RemoveAllChildren(false, out ChildrenRemoved);
    }

    /// <summary>
    /// Remove all children from this ScrollSnap
    /// </summary>
    /// <param name="WorldPositionStays">If true, the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before</param>
    /// <param name="ChildrenRemoved">Array of child GO's removed</param>
    public void RemoveAllChildren(bool WorldPositionStays, out GameObject[] ChildrenRemoved)
    {
      var _screenCount = content.childCount;
      ChildrenRemoved = new GameObject[_screenCount];

      for (int i = _screenCount - 1; i >= 0; i--)
      {
        ChildrenRemoved[i] = content.GetChild(i).gameObject;
        ChildrenRemoved[i].transform.SetParent(null, WorldPositionStays);
      }

      horizontalNormalizedPosition = 0;
      SetCurrentPage(0);
      InitialiseChildObjectsFromScene();
      DistributePages();
      if (MaskArea) UpdateVisible();
    }

    private void SetScrollContainerPosition()
    {
      _scrollStartPosition = content.localPosition.x;
      horizontalNormalizedPosition = (float)(_currentPage) / (_screens - 1);
      OnCurrentScreenChange(_currentPage);
    }

    /// <summary>
    /// used for changing / updating between screen resolutions
    /// </summary>
    public void UpdateLayout()
    {
      panelDimensions = gameObject.GetComponent<RectTransform>().rect;
      _lerp = false;
      DistributePages();
      if (MaskArea) UpdateVisible();
      SetScrollContainerPosition();
      OnCurrentScreenChange(_currentPage);
    }

    private new void OnRectTransformDimensionsChange()
    {
      if (_childAnchorPoint != Vector2.zero)
      {
        UpdateLayout();
      }
    }

    private new void OnEnable()
    {
      InitialiseChildObjectsFromScene();
      DistributePages();
      if (MaskArea) UpdateVisible();

      if (JumpOnEnable || !RestartOnEnable) SetScrollContainerPosition();
      if (RestartOnEnable) GoToScreen(StartingScreen);
    }

    #region Interfaces
    /// <summary>
    /// Release screen to swipe
    /// </summary>
    /// <param name="eventData"></param>
    public override void OnEndDrag(PointerEventData eventData)
    {
      base.OnEndDrag(eventData);

      _pointerDown = false;

      if (horizontal)
      {
        var distance = Vector3.Distance(_startPosition, content.localPosition);
        if (UseFastSwipe && distance < panelDimensions.width && distance >= FastSwipeThreshold)
        {
          velocity = Vector3.zero;
          if (_startPosition.x - content.localPosition.x > 0)
          {
            NextScreen();
          }
          else
          {
            PreviousScreen();
          }
        }
      }
    }
    #endregion
  }
}