﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SOrca.Flibustiers.Client.Assets.Imported_Systems.Layout
{
  [ExecuteInEditMode]
  [RequireComponent(typeof(AspectRatioFitter), typeof(LayoutElement))]
  public class LayoutElementFitter : UIBehaviour
  {
    [SerializeField] private AspectRatioFitter _fitter;
    [SerializeField] private LayoutElement _layout;

    protected override void Awake()
    {
      if (_fitter == null) _fitter = GetComponent<AspectRatioFitter>();
      if (_layout == null) _layout = GetComponent<LayoutElement>();
      SetHeight();
    }

    protected override void OnRectTransformDimensionsChange()
    {
      SetHeight();
    }

    private void SetHeight()
    {
      var rect =  GetComponent<RectTransform>();
      if (rect == null || _layout == null || _fitter == null) return;
      switch (_fitter.aspectMode)
      {
        case AspectRatioFitter.AspectMode.HeightControlsWidth:
          _layout.preferredWidth = rect.rect.height/_fitter.aspectRatio;
          break;
        case AspectRatioFitter.AspectMode.WidthControlsHeight:
          _layout.preferredHeight = rect.rect.width/_fitter.aspectRatio;
          break;
        case AspectRatioFitter.AspectMode.FitInParent:
            _layout.preferredHeight = rect.rect.width/_fitter.aspectRatio;
          break;
      }
    }

#if UNITY_EDITOR
    protected override void OnValidate()
    {
      SetHeight();
    }
#endif
  }
}
