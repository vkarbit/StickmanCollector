namespace UI.HUD
{
    public class DarkeningPanelWindowHudMediator : HudMediator<DarkeningPanelWindowHudView>
    {
        protected override void OnMediate()
        {
            base.OnMediate();

            View.DarkeningPanel.Init(Services);

            Services.DarkeningPanel.Opened += View.DarkeningPanel.Show;
            Services.DarkeningPanel.Closed += View.DarkeningPanel.Hide;
        }

        protected override void OnUnMediate()
        {
            base.OnUnMediate();

            Services.DarkeningPanel.Opened -= View.DarkeningPanel.Show;
            Services.DarkeningPanel.Closed -= View.DarkeningPanel.Hide;
        }
    }
}
