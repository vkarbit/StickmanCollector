﻿using Base;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace UI
{
    public class DarkeningPanel : MonoBehaviour
    {
        [SerializeField] private float _colorTweenTime;
        [SerializeField] private GameObject _darkeningPanel;
        [SerializeField] private Canvas _canvas;
        [SerializeField] private Image _darkeningImage;

        private Tween _darkeningTween;
        private bool _isBlock;

        private Services _services;

        public void Init(Services services)
        {
            _services = services;
            _darkeningImage.DOFade(0, 0);
        }

        public void Show()
        {
            KillTween();

            _darkeningImage.enabled = true;
            _darkeningImage.DOFade(0, 0);
            _darkeningPanel.SetActive(true);
            _darkeningTween = _darkeningImage.DOFade(0.7f, _colorTweenTime);
        }

        private void KillTween(bool complete = false)
        {
            if (_darkeningTween == null || !_darkeningTween.IsActive()) return;

            _darkeningTween.Pause();
            _darkeningTween.Kill(complete);
        }

        public void Hide()
        {
            KillTween();

            _darkeningTween = _darkeningImage.DOFade(0, _colorTweenTime);

            if (_isBlock) return;

            _darkeningTween.OnComplete(HideOnComplete);
        }

        private void HideOnComplete()
        {
            _darkeningImage.enabled = false;
            _darkeningPanel.SetActive(false);

            _services.HUD.Hide(HUDName.DarkeningPanelWindowHud);
        }
    }
}