using UnityEngine;

namespace UI.HUD
{
    public class DarkeningPanelWindowHudView : HudView
    {
        [SerializeField] private DarkeningPanel _darkeningPanel;
        public DarkeningPanel DarkeningPanel => _darkeningPanel;
    }
}
