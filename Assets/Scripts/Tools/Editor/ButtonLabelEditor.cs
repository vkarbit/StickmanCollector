using UnityEngine.UI;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(ButtonLabel))]
    public class ButtonLabelEditor : ButtonEditor
    {
        private SerializedProperty _labelProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            _labelProperty = serializedObject.FindProperty("_label");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            serializedObject.Update();
            EditorGUILayout.ObjectField(_labelProperty);
            serializedObject.ApplyModifiedProperties();      
        }
    }
}