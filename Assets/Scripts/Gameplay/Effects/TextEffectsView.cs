using DG.Tweening;
using UnityEngine;

public class TextEffectsView : MonoBehaviour
{
    [SerializeField] private GameObject _text;
    [SerializeField] private Transform _pointToSpawn;
    [SerializeField] private Transform _pointToMove;
    private Vector3 _rotation = new Vector3(0f, 0f, 0f);

    public void Show()
    {
        transform.Rotate(0, 0, 0);
        _text.transform.rotation = Quaternion.Euler(_rotation);
        _pointToSpawn.transform.position = _pointToSpawn.transform.position;
        _text.transform.DOScale(new Vector3(0.003f, 0.003f, 0.003f), 0f);
        _text.SetActive(true);
        PlayAnim();
    }

    private void PlayAnim()
    {
        _text.transform.DOScale(new Vector3(0.03f, 0.03f, 0.03f), 1f).OnComplete(() => _text.SetActive(false));
    }

        private void Update()
    {
        if (_text.transform.rotation.eulerAngles.y != 0)
        {
            _text.transform.rotation = Quaternion.Euler(_rotation);
        }
    }
}
