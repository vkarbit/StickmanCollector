using System.Collections;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    [SerializeField] private FruitsEnum _fruitsEnum;
    [SerializeField] private float _moveSpeed = 1;
    public FruitsEnum FruitsEnum => _fruitsEnum;

    private Vector3 _initialPosition;
    private bool _isMoving = false;
    public bool IsMoving => _isMoving;

    private void OnEnable()
    {
        _initialPosition = transform.position;
        StartCoroutine(MoveObject());
    }
    private void OnDisable()
    {
        _isMoving = false;
        StopCoroutine(MoveObject());
    }

    private IEnumerator MoveObject()
    {
        _isMoving = true;

        while (_isMoving)
        {
            float targetX = transform.position.x - (_moveSpeed * Time.deltaTime);

            transform.position = new Vector3(targetX, transform.position.y, transform.position.z);

            yield return null;
        }
    }


    public void StopMoving()
    {
        _isMoving = false;
    }

}
