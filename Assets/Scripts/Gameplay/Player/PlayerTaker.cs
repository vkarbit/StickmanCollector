using DG.Tweening;
using UnityEngine;

public class PlayerTaker
{
    private bool _isTake;
    private Camera _mainCamera;
    private Transform _leftHandIK;
    private PlayerAnimation _playerAnimation;

    public bool IsTake => _isTake;

    public PlayerTaker(Transform leftHandIK, PlayerAnimation playerAnimation)
    {
        _playerAnimation = playerAnimation;
        _leftHandIK = leftHandIK;
        _mainCamera = Object.FindObjectOfType<GameCamera>().GetComponent<Camera>();
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Item")))
            {
                float distance = Vector3.Distance(_leftHandIK.position, hit.collider.gameObject.transform.position);
                if (distance >= 0.40f && distance <= 0.70f)
                {
                    _isTake = true;
                    _leftHandIK.DOMove(hit.collider.gameObject.transform.position, 0.1f).OnComplete(() =>
                    {
                        _playerAnimation.Take(true);
                    });
                    DOVirtual.DelayedCall(0.65f, () =>
                    {
                        _playerAnimation.Take(false);
                        _isTake = false;
                    });
                }
            }
        }
    }


}
