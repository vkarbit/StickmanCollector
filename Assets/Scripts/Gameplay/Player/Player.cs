using DG.Tweening;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class Player : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform _rightHandIK;
    [SerializeField] private Transform _leftHandIK;
    [SerializeField] private PlayerHandTrigger _playerHandTrigger;
    [SerializeField] private Basket _basket;
    [SerializeField] private Rig _rig;
    [SerializeField] private TextEffectsView _textEffectsView;

    private PlayerAnimation _playerAnimation;
    private PoolManager _poolManager;
    private GameService _gameService;
    private ChallengesService _challengesService;
    private PlayerTaker _playerTaker;
    private bool _isUpdate = true;
    public PlayerAnimation PlayerAnimation => _playerAnimation;

    public void Initialize(PoolManager poolManager, ChallengesService challengesService, GameService gameService)
    {
        _poolManager = poolManager;
        _gameService = gameService;
        _challengesService = challengesService;
        _playerAnimation = new PlayerAnimation(_animator);
        _playerTaker = new PlayerTaker(_leftHandIK, _playerAnimation);

        _playerHandTrigger.Init(_playerTaker, poolManager);
        _basket.Init(challengesService, _textEffectsView);

        _rightHandIK.localPosition = new Vector3(0.69f, 0.773f, 0.123f);
        //_rightHandIK.localPosition = new Vector3(0.903f, 1.062f, 0.229f);
        _rightHandIK.localRotation = new Quaternion(1.831f, -23.024f, -155.769f, 0);
        //_rightHandIK.localRotation = new Quaternion(-39.426f, 2.818f, -155.769f, 0);

        _challengesService.OnChallengeCompleted += OnChallengeCompleted;
        _gameService.OnRestart += OnRestart;
    }

    private void OnDestroy()
    {
        _challengesService.OnChallengeCompleted -= OnChallengeCompleted;
        _gameService.OnRestart -= OnRestart;
    }

    private void Update()
    {
        if (_isUpdate == false) return;

        _playerTaker.Update();
    }

    private void OnChallengeCompleted()
    {
        _gameService.IsSpawn = false;
        _playerHandTrigger.CanTake = false;
        DOVirtual.DelayedCall(0.5f, () =>
        {
            _rig.weight = 0;

            _basket.ClearBasket();
            _basket.gameObject.SetActive(false);

            _playerAnimation.Dancing(true);
            _poolManager.Pool.ReturnAllElementsToPool();
        });
    }

    private void OnRestart()
    {
        _gameService.IsSpawn = true;
        _playerAnimation.Dancing(false);
        _rig.weight = 1;

        _basket.gameObject.SetActive(true);

        DOVirtual.DelayedCall(0.7f, () =>
        {
            _playerHandTrigger.CanTake = true;

        });
    }
}
