using UnityEngine;

public class PlayerAnimation
{
    private Animator _animator;

    public PlayerAnimation(Animator animator)
    {
        _animator = animator;
    }

    public void Dancing(bool value)
    {
        _animator.SetBool("Dance", value);
    }

    public void Take(bool value)
    {
        _animator.SetBool("Take", value);
    }
}
