using DG.Tweening;
using UnityEngine;

public class PlayerHandTrigger : MonoBehaviour
{
    private PlayerTaker _playerTaker;
    private PoolManager _poolManager;
    private bool _isPut = true;

    public bool CanTake = true;
    public void Init(PlayerTaker playerTaker, PoolManager poolManager)
    {
        _playerTaker = playerTaker;
        _poolManager = poolManager;
    }

    private void OnTriggerStay(Collider other)
    {
        if(CanTake == false) return;

        if (_playerTaker.IsTake)
        {
            if (_isPut == false) return;

            if (other.gameObject.TryGetComponent(out Fruit fruit))
            {
                _poolManager.Pool.RemoveElementFromPool(fruit);
                fruit.StopMoving();
                fruit.gameObject.transform.SetParent(transform);
                fruit.gameObject.transform.DOMove(transform.position, 0.1f);
                _isPut = false;
            }
        }
        else
        {
            _isPut = true;
        }
    }

}
