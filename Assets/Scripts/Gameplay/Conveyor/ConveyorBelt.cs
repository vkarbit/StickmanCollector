using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    private readonly float _resetOffset = 100f;

    [SerializeField] private float _speed = 1f;

    private Material _material;
    private float _currentOffset;

    private void Start()
    {
        _material = GetComponent<Renderer>().material;
        _currentOffset = 0f;
    }

    private void Update()
    {
        _currentOffset += _speed * Time.deltaTime;
        if (_currentOffset >= _resetOffset)
        {
            _currentOffset = 0f;
        }
        _material.SetTextureOffset("_MainTex", new Vector2(_currentOffset, 0f));
    }
}
