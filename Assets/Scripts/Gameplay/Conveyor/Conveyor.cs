using System.Collections;
using UnityEngine;

public class Conveyor : MonoBehaviour
{
    [SerializeField] private ConveyorTrigger _conveyorTrigger;
    private PoolManager _poolManager;
    private GameService _gameService;
    private int _randomNumber;
    private Fruit _fruit;

    [SerializeField] private Transform _pointToSpawn;
    public void Initialize(PoolManager poolManager, GameService gameService)
    {
        _poolManager = poolManager;
        _gameService = gameService;
        _conveyorTrigger.Init(poolManager);

        StartCoroutine(SpawnObj());
    }

    private void OnDestroy()
    {
        StopCoroutine(SpawnObj());
    }

    private IEnumerator SpawnObj()
    {
        while (true)
        {
            if (_gameService.IsSpawn)
            {
                _randomNumber = Random.Range(1, 4);

                _fruit = _poolManager.Pool.GetFreeElement((FruitsEnum)_randomNumber);
                _fruit.transform.position = _pointToSpawn.position;
            }

            yield return new WaitForSeconds(1.75f);
        }
    }
}
