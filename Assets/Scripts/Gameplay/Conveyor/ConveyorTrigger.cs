using UnityEngine;

public class ConveyorTrigger : MonoBehaviour
{
    private PoolManager _poolManager;
    public void Init(PoolManager poolManager)
    {
        _poolManager = poolManager;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.TryGetComponent(out Fruit fruit))
        {
            _poolManager.Pool.ReturnElementToPool(fruit);
        }
    }
}
