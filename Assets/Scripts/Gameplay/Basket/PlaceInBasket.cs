using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceInBasket : MonoBehaviour
{
    private bool _isEmpty = true;

    public bool IsEmpty => _isEmpty;

    public void Occupied()
    {
        _isEmpty = false;
    }

    public void Clear()
    {
        Destroy(transform.GetChild(0).gameObject);
        _isEmpty = true;
    }
}
