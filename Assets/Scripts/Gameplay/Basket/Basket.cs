using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class Basket : MonoBehaviour
{
    [SerializeField] private List<PlaceInBasket> _places;
    private ChallengesService _challengesService;
    private TextEffectsView _textEffectsView;

    public void Init(ChallengesService challengesService, TextEffectsView textEffectsView)
    {
        _textEffectsView = textEffectsView;
        _challengesService = challengesService;

        _challengesService.SetMaxBasketCapasity(_places.Count);
    }

    private void SendToPlace(Transform transform, FruitsEnum fruitsEnum)
    {
        foreach (var place in _places)
        {
            if (place.IsEmpty)
            {
                transform.DOMove(place.transform.position, 0.2f).OnComplete(() =>
                {
                    if (_challengesService.CurrentChallenge.FruitsEnum == fruitsEnum)
                    {
                        _challengesService.CurrentChallenge.AddProgress(1);
                        _challengesService.SetCurrentBasketCapasity(1);
                        _textEffectsView.Show();
                    }
                    else
                    {
                        _challengesService.SetCurrentBasketCapasity(1);
                        _challengesService.CheskLose();
                    }
                    transform.SetParent(place.transform);
                    _challengesService.OnAddNewItem?.Invoke(_challengesService.CurrentBasketCapasity);
                });
                place.Occupied();
                break;
            }
            else
            {

            }
        }
    }

    public void ClearBasket()
    {
        foreach (var place in _places)
        {
            if (place.IsEmpty == false)
            {
                place.Clear();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out Fruit fruit))
        {
            if (fruit.IsMoving) return;

            fruit.gameObject.transform.SetParent(transform);
            SendToPlace(fruit.gameObject.transform, fruit.FruitsEnum);
        }
    }
}
