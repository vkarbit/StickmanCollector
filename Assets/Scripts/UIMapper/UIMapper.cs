using System.Collections.Generic;
using System;
using Base;
using UI;
using UnityEngine;

namespace UIMapper
{
    public abstract class UIMapper<TKey, TMediator, TView>
        where TMediator : BaseMediator
        where TView : BaseView
    {
        private readonly Dictionary<TKey, Type> _map = new Dictionary<TKey, Type>();

        protected void Map<TK>(TKey key) where TK : TMediator
        {
            _map.Add(key, typeof(TK));
        }

        public TMediator Get(TKey key, GameObject viewGO, Services services)
        {
            if (_map.TryGetValue(key, out var type) && viewGO.TryGetComponent<TView>(out var view))
            {
                var mediator = Activator.CreateInstance(type) as TMediator;
                mediator?.Initialize(view, services);

                return mediator;
            }

            Debug.LogError($"{GetType().Name} doesn't contain key {key} or view doesn't have {typeof(TView).Name} component");
            return null;
        }
    }
}