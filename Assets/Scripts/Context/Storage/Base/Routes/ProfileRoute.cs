namespace Base.Storage
{
    public class ProfileRoute : StorageRoute
    {
        public override string Folder => "Profile";
    }
}