namespace Base.Storage
{
    public abstract class StorageRoute
    {
        public virtual string LoadPath => UnityEngine.Application.persistentDataPath;
        public virtual string SavePath => UnityEngine.Application.persistentDataPath;
        public virtual string Folder => "";
        public virtual string Extension => "json";
    }
}