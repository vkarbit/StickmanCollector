using System.Collections.Generic;

namespace Base
{
    public class ProcessesService : Service
    {
        protected readonly List<Process> _processes = new List<Process>();
 
        public void Run(Process process)
        {
            if (process == null) return;
            
            _processes.Add(process.OnComplete(Remove));
            
            process.Run(Services);
        }

        private void Remove(Process process)
        {
            _processes.Remove(process);
        }

        protected override void OnDispose()
        {
            for (int i = 0; i < _processes.Count; i++)
            {
                _processes[i].ForceComplete();
            }
            
            _processes.Clear();
        }
    }
}