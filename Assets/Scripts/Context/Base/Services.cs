using System.Collections.Generic;

namespace Base
{
    public class Services : ServiceInitializer
    {
        private readonly List<Service> _services;
        private ServiceInitializer _initializer;

        public UnityBridgeService UnityBridge { get; }
        public ResourcesService Resources { get; }
        public GameStateService GameState { get; }
        public ScenesService Scene { get; }
        public ProcessesService Processes { get; }
        public UILayersService UILayers { get; }
        public WindowsService Windows { get; }
        public HudService HUD { get; }
        public UILinkService UILink { get; }
        public DarkeningPanelService DarkeningPanel { get; }
        public GameService Game { get; }
        public ChallengesService Challenges { get; }


        public Services()
        {
            _services = new List<Service>();
            
            Add(UnityBridge = new UnityBridgeService());
            Add(Resources = new ResourcesService());
            Add(GameState = new GameStateService());
            Add(Scene = new ScenesService());
            Add(Processes = new ProcessesService());
            Add(UILayers = new UILayersService());
            Add(Windows = new WindowsService());
            Add(HUD = new HudService());
            Add(UILink = new UILinkService());
            Add(DarkeningPanel = new DarkeningPanelService());
            Add(Game = new GameService());
            Add(Challenges = new ChallengesService());
        }

        private void Add(Service service)
        {
            _services.Add(service);
        }

        public void Run()
        {
            _initializer = new ServiceInitializer();

            foreach (var service in _services)
            {
                service.InitializeService(_initializer);
            }
            
            _initializer.Run(this);
        }

        public void Dispose()
        {
            foreach (var service in _services)
            {
                service.Dispose();
            }
        }
    }
}