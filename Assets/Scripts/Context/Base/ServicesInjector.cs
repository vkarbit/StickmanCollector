namespace Base
{
    public abstract class ServicesInjector
    {
        protected Services Services { get; private set; }

        protected ServicesInjector() { }
        
        protected ServicesInjector(Services services)
        {
            Inject(services);
        }

        protected void Inject(Services services)
        {
            Services = services;
        }
    }
}