﻿using System;
using System.Collections;
using UnityEngine;

namespace Base
{
    public class ServicesInitializerBehaviour : MonoBehaviour, ICoroutineRunner
    {
        [SerializeField] private UILayers _uiLayers;

        private Services _services;

        private void Awake()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 61;

            DontDestroyOnLoad(gameObject);
        }

        private IEnumerator Start()
        {
            if (!Application.isEditor)
            {
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }

            _services = new Services();
            _services.Run();
            _services.UILayers.Install(_uiLayers);
        }

        public void DoAfter(Func<bool> condition, Action action) => 
            StartCoroutine(DoAfterCoroutine(condition, action));

        private IEnumerator DoAfterCoroutine(Func<bool> condition, Action action)
        {
            yield return new WaitUntil(condition);

            action.Invoke();
        }


        private void OnDestroy()
        {
            _services?.Dispose();
        }
    }
}