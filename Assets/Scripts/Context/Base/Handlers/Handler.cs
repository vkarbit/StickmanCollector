namespace Base
{
    public abstract class Handler : ServicesInjector
    {
        protected HandlersHolder Handlers { get; private set; }
        
        public void InitializeService(HandlerInitializer serviceInitializer)
        {
            serviceInitializer.Inject += base.Inject;
            serviceInitializer.Initialize += Initialize;
        }
        
        private void Initialize()
        {
            Handlers = new HandlersHolder(Services);
            BindHandlers();
            Handlers.Initialize();
            
            OnInitialize();
        }

        protected virtual void BindHandlers() { }
        
        protected virtual void OnInitialize() { }

        public void Dispose()
        {
            Handlers.Dispose();
            OnDispose();
        }

        protected virtual void OnDispose() { }
    }

    public static class HandlerExtension
    {
        public static void SafeDispose(this Handler handler)
        {
            handler?.Dispose();
        }
    }
}