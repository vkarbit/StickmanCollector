using System;

namespace Base
{
    public abstract class DataHandler<T> : Handler
    {
        public DataHandler(T data, Action saveCallback)
        {
            Data = data;
            _save = saveCallback;
        }

        private event Action _save;
        
        protected T Data { get; }

        protected void Save()
        {
            _save?.Invoke();
        }
    }
}