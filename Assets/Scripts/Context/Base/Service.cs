namespace Base
{

    public abstract class Service : ServicesInjector
    {
        protected HandlersHolder Handlers { get; private set; }

        public void InitializeService(ServiceInitializer serviceInitializer)
        {
            serviceInitializer.Inject += InjectServices;
            serviceInitializer.Initialize += Initialize;
            serviceInitializer.BindHandlers += BindHandlers;
            serviceInitializer.PostInitialize += PostInitialize;
            
            OnInitializeService(serviceInitializer);
            
            Handlers = new HandlersHolder(Services);
        }

        protected virtual void OnInitializeService(ServiceInitializer serviceInitializer) { }

        private void InjectServices(Services services)
        {
            base.Inject(services);
            Handlers = new HandlersHolder(services);
        }
        
        private void Initialize()
        {
            Handlers.Initialize();
            
            OnInitialize();
        }

        protected virtual void BindHandlers() {}
        
        protected virtual void OnInitialize() {}

        private void PostInitialize()
        {
            OnPostInitialize();
        }
        
        protected virtual void OnPostInitialize() {}
        
        public void Dispose()
        {
            Handlers.Dispose();
            OnDispose();
        }

        protected virtual void OnDispose() { }
    }
}