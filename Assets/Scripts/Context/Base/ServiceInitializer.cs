using System;

namespace Base
{
    public class ServiceInitializer
    {
        public event Action<Services> Inject;
        public event Action InitializeData;
        public event Action BindHandlers;
        public event Action Initialize;
        public event Action PostInitialize;

        public void Run(Services services)
        {
            Inject?.Invoke(services);
            InitializeData?.Invoke();
            BindHandlers?.Invoke();
            Initialize?.Invoke();
            PostInitialize?.Invoke();
        }
    }
}