using Base;
using System;

public class GameService : Service
{
    public Action OnRestart;

    public bool IsSpawn = true;

    protected override void BindHandlers()
    {
        base.BindHandlers();

        Handlers.Add(_ = new MenuInitializationHandler());
        Handlers.Add(_ = new GameInitializationHandler());
    }
}
