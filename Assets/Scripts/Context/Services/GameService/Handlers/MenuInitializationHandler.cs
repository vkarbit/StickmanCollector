using Base;

public class MenuInitializationHandler : Handler
{
    protected override void OnInitialize()
    {
        base.OnInitialize();

        Services.GameState.TransitionRequiredAction += TryInitialize;
    }

    private void TryInitialize(GameState state)
    {
        if (state == GameState.Menu)
        {
            Initialize();
        }
    }

    private void Initialize()
    {
        //TODO
    }

    protected override void OnDispose()
    {
        base.OnDispose();

        Services.GameState.TransitionRequiredAction -= TryInitialize;
    }
}
