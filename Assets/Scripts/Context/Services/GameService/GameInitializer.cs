using Base;
using GamePlay.Managers;
using UnityEngine;

public class GameInitializer : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    public Camera Camera => _camera;

    public ManagersContext Managers;
    public void Initialize(Services services)
    {
        Managers = new ManagersContext(services);

        Managers.Initialize();

        Managers.PostInitialize();

        Managers.Camera.SetCamera(Camera);
    }

    private void Dispose()
    {
        Managers.Dispose();
    }

    private void OnDestroy()
    {
        Dispose();
    }
}
