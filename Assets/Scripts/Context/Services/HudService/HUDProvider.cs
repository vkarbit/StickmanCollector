using UI;
using UIMapper;

namespace Base.HUD
{
    public class HUDProvider : ServicesInjector
    {
        private readonly HudMapper _map = new HudMapper();

        public HUDProvider(Services services) : base(services)
        {
        }

        public HudMediator Get(HUDName name)
        {
            if (Services.Resources.HUD.TryGetInstance(name, out var go))
            {
                Services.UILayers.Add(UILayer.HUD, go);
                var mediator = _map.Get(name, go, Services);
                mediator.SetName(name);

                return mediator;
            }

            return null;
        }
    }
}