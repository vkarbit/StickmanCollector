public enum SceneName
{
    Entrance = 1,
    SplashScreen = 2,
    Menu = 3,
    Game = 4
}