using System;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{
    public class UILinkService : Service
    {
        private readonly Dictionary<UILinkKey, Component> _linksMap = new Dictionary<UILinkKey, Component>();
        
        public event Action<UILinkKey, Component> Linked;

        public void Link(UILinkKey key, Component component)
        {
            if (_linksMap.ContainsKey(key))
            {
                _linksMap.Remove(key);
            }
            _linksMap.Add(key, component);
            Linked?.Invoke(key, component);
        }

        public void Unlink(UILinkKey key)
        {
            _linksMap.Remove(key);
        }

        public bool HasKey(UILinkKey key)
        {
            return _linksMap.ContainsKey(key) && null != _linksMap[key];
        }

        public Component GetComponent(UILinkKey key)
        {
            return (_linksMap.ContainsKey(key)) ? _linksMap[key] : null;
        }

        public T GetComponent<T>(UILinkKey key) where T : Component
        {
            return (T) GetComponent(key);
        }

        protected override void OnDispose()
        {
            _linksMap.Clear();
        }
    }
}