﻿using System;
using UnityEngine;

namespace Base
{
    public class UnityBridgeService : Service
    {
        public event Action Tick;
        public event Action FixedTick;
        public event Action Pause;
        public event Action Resume;
        public event Action Quit;
        public event Action Focus;
        public event Action Unfocus;
        
        private UnityBridgeBehaviour _behaviour;

        protected override void OnInitialize()
        {
            base.OnInitialize();
            
            _behaviour = new GameObject("UnityBridgeBehaviour").AddComponent<UnityBridgeBehaviour>();
            _behaviour.Initialize(OnTick, OnFixedTick, OnFocus, OnPause, OnQuit);
        }

        private void OnTick()
        {
            Tick?.Invoke();
        }

        private void OnFixedTick()
        {
            FixedTick?.Invoke();
        }

        private void OnFocus(bool focus)
        {
            if (focus)
            {
                OnFocus();
                return;
            }

            OnUnfocus();
        }
        
        private void OnFocus()
        {
            Focus?.Invoke();
        }        
        
        private void OnUnfocus()
        {
            Unfocus?.Invoke();
        }
        
        private void OnPause(bool pause)
        {
            if (pause)
            {
                OnPause();
                return;
            }

            OnResume();
        }
        
        private void OnPause()
        {
            Pause?.Invoke();
        }

        private void OnResume()
        {
            Resume?.Invoke();
        }

        private void OnQuit()
        {
            Quit?.Invoke();
        }

        protected override void OnDispose()
        {
            if (_behaviour != null)
            {
                UnityEngine.Object.DestroyImmediate(_behaviour.gameObject);
            }
        }
    }
}

