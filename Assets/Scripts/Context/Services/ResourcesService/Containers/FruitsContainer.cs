using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Base
{
    [CreateAssetMenu(fileName = "FruitsContainer", menuName = "ScriptableObjects/Other/FruitsContainer")]
    public class FruitsContainer : ScriptableObject
    {
        [SerializeField] private FruitsData _fruitsData;

        public FruitsData FruitsData => _fruitsData;

        public Fruit GetElementByType(FruitsEnum type)
        {
            foreach(var fruit  in _fruitsData.Fruits)
            {
                if(fruit.FruitsEnum == type) return fruit;
            }

            return null;
        }
    }

    [Serializable]
    public class FruitsData
    {
        [SerializeField] private List<Fruit> _fruits;
        public List<Fruit> Fruits => _fruits;
    }
}