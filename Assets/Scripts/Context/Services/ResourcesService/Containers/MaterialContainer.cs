using UnityEngine;

namespace Base
{
    [CreateAssetMenu(fileName = "PrefabContainer", menuName = "ScriptableObjects/Other/MaterialContainer")]
    public class MaterialContainer : ScriptableObject
    {
        [SerializeField] private Material _material;

        public Material Material => _material;
    }

}
