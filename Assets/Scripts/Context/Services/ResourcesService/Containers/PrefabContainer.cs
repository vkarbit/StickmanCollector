using UnityEngine;

namespace Base
{
    [CreateAssetMenu(fileName = "PrefabContainer", menuName = "ScriptableObjects/Other/PrefabContainer")]
    public class PrefabContainer : ScriptableObject
    {
        [SerializeField] private GameObject _prefab;

        public GameObject Prefab => _prefab;
    }

}
