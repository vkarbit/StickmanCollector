using UnityEngine;

namespace Base.Handlers
{
    public class ResourcePrefabsHandler : ResourceHandler
    {
        protected override string Path => "Prefabs";
    
        public GameObject GetInstance(PrefabName prefabName)
        {
            return GetInstance(prefabName.ToString());
        }
    
        public GameObject GetInstance(string prefabName)
        {
            return Object.Instantiate(Get(prefabName));
        }
        
        public T GetInstance<T>(PrefabName prefabName) where T : Component
        {
            return Object.Instantiate(Get<T>(prefabName));
        }
        public T GetInstance<T>(string prefabName) where T : Component
        {
            return Object.Instantiate(Get<T>(prefabName));
        }

        private T Get<T>(PrefabName prefabName) where T : Component
        {
            var prefab = Get(prefabName.ToString());
            
            return GetComponent<T>(prefab);
        }

        private T Get<T>(string prefabName) where T : Component
        {
            var prefab = Get(prefabName);

            return GetComponent<T>(prefab);
        }

        private T GetComponent<T>(GameObject prefab) where T : Component
        {
            if (prefab == null)
                return null;
            return prefab.GetComponent<T>();
        }
    }
}