using UnityEngine;

namespace Base.Handlers
{
    public class ResourcesHudHandler : ResourcePrefabsHandler
    {
        protected override string Path => "HUD";
        
        public bool TryGetInstance(HUDName prefabName, out GameObject go)
        {
            var prefab = GetInstance(prefabName.ToString());
            if (prefab == null)
            {
                go = null;
                return false;
            }

            go = prefab;
            return true;
        }
    }
}