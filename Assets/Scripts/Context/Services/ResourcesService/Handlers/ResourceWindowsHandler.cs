using UnityEngine;

namespace Base.Handlers
{
    public class ResourceWindowsHandler : ResourcePrefabsHandler
    {
        protected override string Path => "Windows";
        
        public bool TryGetInstance(WindowName prefabName, out GameObject go)
        {
            var prefab = GetInstance(prefabName.ToString());
            if (prefab == null)
            {
                go = null;
                return false;
            }

            go = prefab;
            return true;
        }
    }
}