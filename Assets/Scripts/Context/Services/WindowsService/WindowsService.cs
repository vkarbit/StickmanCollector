using System;
using System.Collections.Generic;
using Base.HUD;
using DG.Tweening;
using UI;
using UnityEngine;

namespace Base
{
    public class WindowsService : Service
    {
        private WindowMediator _current;
        private Stack<WindowMediator> _windowsStack;
        private WindowsProvider _provider;
        private Dictionary<WindowName, WindowMediator> _cache;

        #region ACTIONS

        private readonly OneListener<WindowName> _openListener = new OneListener<WindowName>();

        public event Action<WindowName> Opened
        {
            add { _openListener.Add(value); }
            remove { _openListener.Remove(value); }
        }

        private readonly OneListener<WindowName> _closeListener = new OneListener<WindowName>();

        public event Action<WindowName> Closed
        {
            add { _closeListener.Add(value); }
            remove { _closeListener.Remove(value); }
        }

        private readonly OneListener _closeAllListener = new OneListener();

        public event Action ClosedAll
        {
            add { _closeAllListener.Add(value); }
            remove { _closeAllListener.Remove(value); }
        }

        #endregion

        protected override void OnInitialize()
        {
            base.OnInitialize();

            _provider = new WindowsProvider(Services);
            _cache = new Dictionary<WindowName, WindowMediator>();
            _windowsStack = new Stack<WindowMediator>();
        }

        private bool IsSameWindowOpened(WindowName window)
        {
            return _current != null && _current.Name == window;
        }

        public void ForceOpen(WindowName window, WindowData data = null)
        {
            _windowsStack.Clear();
            if (IsSameWindowOpened(window)) return;

            if (_current != null)
            {
                Close(true);
            }

            OpenWindow(window, data);
        }

        public void Open(WindowName window, WindowData data = null)
        {
            if (IsSameWindowOpened(window)) return;

            if (_current != null)
            {
                _current.Hide();
                _windowsStack.Push(_current);
            }

            OpenWindow(window, data);
        }

        private void OpenWindow(WindowName window, WindowData data = null)
        {

            if (_cache.ContainsKey(window))
            {
                _current = _cache[window];
            }
            else
            {
                _current = _provider.Get(window);

                if (_current == null) return;

                _cache.Add(window, _current);
            }

            _current.SetData(data);
            _current.Show();

            _openListener.SafeInvoke(window);
        }

        public WindowName GetOpenedWindowName
        {
            get
            {
                if (_current == null)
                    return WindowName.None;

                return _current.Name;
            }
        }

        public void CloseTo(WindowName window)
        {
            while (_windowsStack.Count > 0)
            {
                if (_current != null && _current.Name == window)
                {
                    return;
                }
                Close(false);
            }
        }

        public void Close()
        {
            Close(false);
        }

        public void Close(WindowName window)
        {
            if (_current != null && _current.Name == window)
            {
                Close(false);
            }
        }

        private void Close(bool onOpen)
        {
            WindowName closeWindow = WindowName.None;

            if (_current != null)
            {
                closeWindow = _current.Name;
            }

            if (_windowsStack.Count > 0)
            {
                _current = _windowsStack.Pop();
                _current.Show();
            }
            else
            {
                _current = null;
            }

            _closeListener.SafeInvoke(closeWindow);

            Debug.Log("[Windows][Close] window " + closeWindow);
            if (closeWindow != WindowName.None && _cache.ContainsKey(closeWindow))
            {
                _cache[closeWindow].Hide();
            }

            if (_current != null)
            {
                _openListener.SafeInvoke(_current.Name);
            }
            else
            {
                //if (!onOpen) _closeAllListener.Invoke();
            }
        }

        public void CloseAll(Action onComplete)
        {
            CloseAll();
            onComplete?.Invoke();
        }

        public void CloseAll()
        {
            HideCurrentWindow();

            while (_windowsStack.Count > 0)
            {
                _windowsStack.Pop().Hide();
            }

            _windowsStack.Clear();
            _closeAllListener.Invoke();
        }

        private void HideCurrentWindow()
        {
            if (_current == null) return;
            _current.Hide();
            _current = null;
        }
    }
}