using System;
using System.Collections.Generic;

namespace Base
{
    public class GameStateMapper<TState, TEvent>
        where TState : GameState
        where TEvent : Type
    {
        private readonly Dictionary<TState, Dictionary<TState, TEvent>> _statesMap = new Dictionary<TState, Dictionary<TState, TEvent>>();

        private TState _currentState;
        private TState _prevState;
        private TEvent _defaultEvent;

        private readonly OneListener<GameState, GameState> _onTransition = new OneListener<GameState, GameState>();
        public event Action<GameState, GameState> ON_TRANSITION
        {
            add { _onTransition.Add(value); }
            remove { _onTransition.Remove(value); }
        }
        
        public TState CurrentState
        {
            get { return _currentState; }
            private set
            {
                _prevState = _currentState;
                _currentState = value;
                
                _onTransition?.Invoke(_prevState, _currentState);
            }
        }

        public TState PrevState
        {
            get { return _prevState; }
        }

        public GameStateMapper(TState startState, TEvent defaultEvent)
        {
            CurrentState = startState;
            _defaultEvent = defaultEvent;
        }

        public void Map(TState from, TState to, TEvent callback)
        {
            if (!_statesMap.ContainsKey(from))
            {
                _statesMap.Add(from, new Dictionary<TState, TEvent>());
            }

            if (_statesMap[from].ContainsKey(to))
            {
                _statesMap[from].Remove(to);
            }

            _statesMap[from].Add(to, callback);
        }
        
        public TEvent this[TState to]
        {
            get
            {
                if (_statesMap.ContainsKey(_currentState) && _statesMap[_currentState].ContainsKey(to))
                {
                    CurrentState = to;
                    return _statesMap[_prevState][to]; 
                }

                UnityEngine.Debug.LogError($"Attempted for unhandled state transition. From state : {_currentState}. To state : {to}.");                
                return _defaultEvent;
            }
        }
    }
}