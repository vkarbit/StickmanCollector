using System;
using Base.GameStates;

namespace Base
{
    public class GameStateService : Service
    {
        public event Action<SceneName> SceneLoaded;
        public event Action<GameState> StartStateChange;
        public event Action<GameState> TransitionStart;
        public event Action<GameState> TransitionRequiredAction;
        public event Action<GameState> TransitionEnd;
        public event Action<GameState> StateChange;
        public event Action OnLevelUnload;

        private GameStateTransition _actionState;
        private SplashScreenHandler _splashScreen;
        private readonly GameStateMapper<GameState, Type> _statesMap = new GameStateMapper<GameState, Type>(GameState.Initialize, typeof(GameStateTransition));

        public GameState Current => _statesMap.CurrentState;

        public GameState Previous { get; set; }

        public GameStateService()
        {
            Map<InitializeToEntrance>(GameState.Initialize, GameState.Entrance);
            Map<EntranceToMenu>(GameState.Entrance, GameState.Menu);
            Map<MenuToGame>(GameState.Menu, GameState.Game);
            Map<GameToMenu>(GameState.Game, GameState.Menu);
        }

        protected override void BindHandlers()
        {
            base.BindHandlers();
            
            Handlers.Add(_splashScreen = new SplashScreenHandler());
        }

        protected override void OnPostInitialize()
        {
            base.OnPostInitialize();
            
            RunTo(GameState.Entrance);
        }


        private void Map<T>(GameState from, GameState to)
        {
            _statesMap.Map(from, to, typeof(T));
        }

        public void RunTo(GameState state)
        {
            ChangeState(state);
        }

        private void ChangeState(GameState state)
        {
            StartStateChange?.Invoke(state);
            
            _actionState?.ForceComplete();

            Previous = Current;
            _actionState = Activator.CreateInstance(_statesMap[state]) as GameStateTransition ?? new EmptyGameStateTransition();
            
            _actionState.InjectSplashScreen(_splashScreen);
            _actionState.TransitionStart += OnTransitionStart;
            _actionState.TransitionEnd += OnTransitionEnd;
            _actionState.TransitionRequiredAction += OnTransitionRequiredAction;
            _actionState.SceneLoaded += OnSceneLoad;
            
            Services.Processes.Run(_actionState?.OnComplete(OnTransitionComplete));
        }

        private void OnTransitionStart()
        {
            TransitionStart?.Invoke(Current);
        }

        private void OnTransitionEnd()
        {
            TransitionEnd?.Invoke(Current);
        }

        private void OnSceneLoad(SceneName scene)
        {
            SceneLoaded?.Invoke(scene);
        }

        private void OnTransitionRequiredAction()
        {
            TransitionRequiredAction?.Invoke(Current);
        }

        private void OnTransitionComplete(bool complete)
        {
            if (complete)
            {
                _actionState = null;
                StateChange?.Invoke(Current);

                if (Current == GameState.Menu && Previous == GameState.Game)
                {
                    OnLevelUnload.Invoke();
                }
            }
        }

        protected override void OnDispose()
        {
        }
    }
}