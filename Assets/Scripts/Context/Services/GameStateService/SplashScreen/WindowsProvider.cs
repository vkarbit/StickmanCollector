using UI;
using UIMapper;

namespace Base
{
    public class SplashScreenProvider : ServicesInjector
    {
        private readonly SplashScreenMapper _map = new SplashScreenMapper();

        public SplashScreenProvider(Services services) : base(services)
        {
        }

        public SplashScreenMediator Get(SplashScreenName splash)
        {
            if (Services.Resources.SplashScreen.TryGetInstance(splash, out var go))
            {
                Services.UILayers.Add(UILayer.OVERLAY, go);
                var mediator = _map.Get(splash, go, Services);
                mediator.SetName(splash);

                return mediator;
            }

            return null;
        }
    }
}