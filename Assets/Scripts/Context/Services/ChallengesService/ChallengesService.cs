using Base.Storage;
using Base;
using System;
using DG.Tweening;
using UnityEngine;
using Unity.Mathematics;

public class ChallengesService : DataService<ChallengesData, ProfileRoute>
{
    protected override string FileName => "ChallengeData";

    private RandomChallenge _currentChallenge;
    private int _maxBasketCapasity = 0;
    private int _currentBasketCapasity = 0;
    public int MaxBasketCapasity => _maxBasketCapasity;
    public int CurrentBasketCapasity => _currentBasketCapasity;
    public RandomChallenge CurrentChallenge => _currentChallenge;

    public Action OnChallengeCompleted;
    public Action<int> OnAddNewItem;

    public ChallengesHandler ChallengesData{ get; private set; }

    protected override void OnInitialize()
    {
        base.OnInitialize();

        OnChallengeCompleted += () => DOVirtual.DelayedCall(3f, () => Services.Windows.Open(WindowName.WinWindow));
    }

    protected override void BindHandlers()
    {
        base.BindHandlers();

        Handlers.Add(ChallengesData = new ChallengesHandler(Data, Storage.Save));
    }

    protected override void OnDispose()
    {
        base.OnDispose();

        OnChallengeCompleted = null;
    }
    public void SetMaxBasketCapasity(int value)
    {
        _maxBasketCapasity = value;
    }
    public void SetCurrentBasketCapasity(int value)
    {
        _currentBasketCapasity = _currentBasketCapasity + value;
    }

    public void CheskLose()
    {
        if (CurrentBasketCapasity == MaxBasketCapasity)
        {
            Services.Windows.Open(WindowName.LoseWindow);
        }
    }

    public void ResetCurrentBasketCapasity()
    {
        _currentBasketCapasity = 0;
    }

    public void SetCurrentChallenge(RandomChallenge currentChallenge)
    {
        _currentChallenge = currentChallenge;
    }

    public RandomChallenge GenerateRandomChallenge()
    {
        RandomChallenge randomChallenge = new RandomChallenge();

        randomChallenge.SetCount(UnityEngine.Random.Range(3, 6));

        randomChallenge.SetNameByType(RandomEnum.GetRandomNonZeroEnumValue<FruitsEnum>());

        return randomChallenge;
    }

}
