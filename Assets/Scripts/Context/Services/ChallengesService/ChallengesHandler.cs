using Base;
using System;

public class ChallengesHandler : DataHandler<ChallengesData>
{
    public ChallengesHandler(ChallengesData data, Action saveCallback) : base(data, saveCallback)
    {
    }
    public int AppleCollected => Data.AppleCollected;
    public int WatermelonCollected => Data.WatermelonCollected;
    public int StrawberryCollected => Data.StrawberryCollected;

    public void ChallengeValueUpdate(FruitsEnum fruitsEnum, int count)
    {
        switch (fruitsEnum)
        {
            case FruitsEnum.Apple:
                Data.AppleCollected = AppleCollected + count;
                Save();
                return;
            case FruitsEnum.Watermelon:
                Data.WatermelonCollected = WatermelonCollected + count;
                Save();
                return;
            case FruitsEnum.Strawberry:
                Data.StrawberryCollected = StrawberryCollected + count;
                Save();
                return;
        }
    }
}
