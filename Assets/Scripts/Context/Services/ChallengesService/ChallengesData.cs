namespace Base
{
    public class ChallengesData
    {
        public ChallengesData()
        {
            AppleCollected = 0;
            StrawberryCollected = 0;
            WatermelonCollected = 0;
        }

        public int AppleCollected { get; set; }
        public int StrawberryCollected { get; set; }
        public int WatermelonCollected { get; set; }
    }
}
