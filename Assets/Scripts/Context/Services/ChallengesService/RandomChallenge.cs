using System;
using UnityEngine;

public class RandomChallenge
{
    public string Name { get; private set; }
    public int CurrentValue { get; private set; }
    public int NeedCount { get; private set; }
    public FruitsEnum FruitsEnum { get; private set; }

    public Action OnCollectedNeedCount;
    public Action<int> OnCurrentValueChanged;
    public Action<FruitsEnum, int> OnCurrentValueUpdate;

    public void SetNameByType(FruitsEnum fruitsEnum)
    {
        FruitsEnum = fruitsEnum;
        Name = $"Collect {fruitsEnum}";
    }

    public void SetCount(int count)
    {
        NeedCount = count;
    }

    public void AddProgress(int value)
    {
        CurrentValue += value;

        OnCurrentValueChanged?.Invoke(CurrentValue);
        OnCurrentValueUpdate?.Invoke(FruitsEnum, value);

        if (CurrentValue == NeedCount)
        {
            OnCollectedNeedCount?.Invoke();
            return;
        }
    }
}