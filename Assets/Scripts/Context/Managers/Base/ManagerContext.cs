using Base;
using System.Collections.Generic;

namespace GamePlay.Managers
{
    public class ManagersContext
    {
        private readonly List<BaseManager> _managers = new List<BaseManager>();

        public FactoryManager Factories { get; }
        public PoolManager Pool { get; }
        public CameraManager Camera { get; }
        public ManagersContext(Services services)
        {
            Add(Factories = new FactoryManager(services.Resources, services.Challenges, services.Game));
            Add(Pool = new PoolManager(services.Resources));
            Add(Camera = new CameraManager(services.Challenges, services.Game));
        }

        private void Add(BaseManager manager)
        {
            _managers.Add(manager);
        }

        public void Initialize()
        {
            foreach (var manager in _managers)
            {
                manager.InitializeObject(this);
            }
        }

        public void PostInitialize()
        {
            foreach (var manager in _managers)
            {
                manager.PostInitialize();
            }
        }

        public void Dispose()
        {
            foreach (var manager in _managers)
            {
                manager.Dispose();
            }
        }

    }
}
