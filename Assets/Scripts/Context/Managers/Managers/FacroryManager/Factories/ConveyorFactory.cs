using Base;

public class ConveyorFactory
{
    private readonly ResourcesService _resourcesService;
    private readonly PoolManager _poolManager;
    private readonly GameService _gameService;

    public ConveyorFactory(ResourcesService resourcesService, PoolManager poolManager, GameService gameService)
    {
        _resourcesService = resourcesService;
        _poolManager = poolManager;
        _gameService = gameService;

        CreateConveyor();
    }

    public void CreateConveyor()
    {
        Conveyor player = _resourcesService.Prefabs.GetInstance<Conveyor>(PrefabName.Conveyor);

        player.Initialize(_poolManager, _gameService);
    }
}
