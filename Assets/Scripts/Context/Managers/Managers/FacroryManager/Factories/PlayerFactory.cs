using Base;

public class PlayerFactory
{
    private readonly ResourcesService _resourcesService;
    private readonly ChallengesService _challengesService;
    private readonly PoolManager _poolManager;
    private readonly GameService _gameService;

    public PlayerFactory(ResourcesService resourcesService, PoolManager poolManager, ChallengesService challengesService, GameService gameService)
    {
        _poolManager = poolManager;
        _resourcesService = resourcesService;
        _challengesService = challengesService;
        _gameService = gameService;

        CreatePlayer();
    }

    public void CreatePlayer()
    {
        Player player = _resourcesService.Prefabs.GetInstance<Player>(PrefabName.Player);

        player.Initialize(_poolManager, _challengesService, _gameService);
    }
}
