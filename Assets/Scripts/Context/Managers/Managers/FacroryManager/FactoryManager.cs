using Base;
using GamePlay.Managers;

public class FactoryManager : BaseManager
{
    public PlayerFactory Player { get; private set; }
    public ConveyorFactory Conveyor { get; private set; }

    private readonly ResourcesService _resourcesService;
    private readonly ChallengesService _challengesService;
    private readonly GameService _gameService;

    public FactoryManager(ResourcesService resourcesService, ChallengesService challengesService, GameService gameService)
    {
        _resourcesService = resourcesService;
        _challengesService = challengesService;
        _gameService = gameService;
    }

    protected override void Initialize()
    {
        base.Initialize();

        InitializePlayerFactory();
        InitializeConveyorFactory();

    }
    public override void Dispose()
    {

    }

    private void InitializePlayerFactory() => Player = new PlayerFactory(_resourcesService, ManagersContext.Pool, _challengesService, _gameService);
    private void InitializeConveyorFactory() => Conveyor = new ConveyorFactory(_resourcesService, ManagersContext.Pool, _gameService);
}
