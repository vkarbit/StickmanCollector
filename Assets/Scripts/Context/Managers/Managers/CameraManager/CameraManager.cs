using GamePlay.Managers;
using UnityEngine;

public class CameraManager : BaseManager
{
    private Camera _camera;
    private Animator _animator;
    private ChallengesService _challengesService;
    private GameService _gameService;
    public CameraManager(ChallengesService challengesService, GameService gameService)
    {
        _challengesService = challengesService;
        _gameService = gameService;
    }

    protected override void Initialize()
    {
        base.Initialize();

        _challengesService.OnChallengeCompleted += MoveToPlayer;
        _gameService.OnRestart += MoveFromPlayer;
    }

    public void SetCamera(Camera camera)
    {
        _camera = camera;
        _animator = _camera.gameObject.GetComponent<Animator>();
    }

    public void MoveToPlayer()
    {
        _animator.SetBool("ToPlayer", true);
    }

    public void MoveFromPlayer()
    {
        _animator.SetBool("ToPlayer", false);
    }

    public override void Dispose()
    {
        _challengesService.OnChallengeCompleted -= MoveToPlayer;
        _gameService.OnRestart -= MoveFromPlayer;
    }
}
