using Base;
using GamePlay.Managers;
using Optimization;
using UnityEngine;

public class PoolManager : BaseManager
{
    private readonly string _fruitsContainerName = "FruitsContainer";
    private GameObject _parentObject;
    public PoolFruits<Fruit> Pool { get; private set; }

    public PoolManager(ResourcesService resourcesService)
    {
        _parentObject = new GameObject("ItemPool");

        FruitsContainer fruitsContainer = resourcesService.ScriptableObjects.Get(FolderName.StaticData, _fruitsContainerName) as FruitsContainer;

        Pool = new PoolFruits<Fruit>(fruitsContainer, fruitsContainer.FruitsData.Fruits, 10, true, _parentObject.transform);
    }

    public override void Dispose()
    {
    }
}
