using Base;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Optimization
{
    public class PoolFruits<T> where T : Fruit
    {
        private readonly Transform _container;
        private readonly List<T> _prefabs;
        private readonly List<T> _freeElements = new();
        private readonly bool _autoExpand;
        private List<T> _pool;
        private readonly FruitsContainer _fruitsContainer;

        public PoolFruits(FruitsContainer fruitsContainer, List<T> prefab, int countInPool, bool autoExpand, Transform container = null)
        {
            _fruitsContainer = fruitsContainer;
            _prefabs = prefab;
            _container = container;
            _autoExpand = autoExpand;
            CreatePool(countInPool);
        }

        private bool HasFreeElement(out T element, FruitsEnum fruitsEnum)
        {
            _freeElements.AddRange(_pool.Where(mono => mono != null && !mono.gameObject.activeInHierarchy && mono.GetComponent<Fruit>().FruitsEnum == fruitsEnum));

            if (_freeElements.Count == 0)
            {
                element = null;
                return false;
            }

            int randomIndex = Random.Range(0, _freeElements.Count);
            element = _freeElements[randomIndex];
            element.gameObject.SetActive(true);
            _freeElements.Clear();
            return true;
        }


        public T GetFreeElement(FruitsEnum fruitsEnum)
        {
            if (HasFreeElement(out var element, fruitsEnum))
            {
                return element;
            }

            if (_autoExpand)
            {
                return CreateObject((T)_fruitsContainer.GetElementByType(fruitsEnum), true);
            }

            throw new System.Exception($"There is no elements in pool of type {typeof(T)}");
        }
        private T CreateObject(T prefab, bool isActiveByDefault = false)
        {
            var createdObj = Object.Instantiate(prefab, _container);
            createdObj.gameObject.SetActive(isActiveByDefault);
            _pool.Add(createdObj);
            return createdObj;
        }

        private void CreatePool(int countInPool)
        {
            _pool = new List<T>();

            CreateObjects(false, countInPool);
        }
        private void CreateObjects(bool isActiveByDefault, int countInPool)
        {
            for (int i = 0; i < _prefabs.Count; i++)
            {
                for (int j = 0; j < countInPool; j++)
                {
                    var createdObj = Object.Instantiate(_prefabs[i], _container);
                    createdObj.gameObject.SetActive(isActiveByDefault);
                    _pool.Add(createdObj);
                }
            }
        }

        public void ReturnElementToPool(T element)
        {
            if (_pool.Contains(element))
            {
                element.gameObject.SetActive(false);
                _freeElements.Add(element);
            }
        }

        public void RemoveElementFromPool(T element)
        {
            if (_pool.Contains(element))
            {
                _pool.Remove(element);
            }
            if (_freeElements.Contains(element))
            {
                _freeElements.Remove(element);
            }
        }

        public void ReturnAllElementsToPool()
        {
            foreach (var element in _pool)
            {
                element.gameObject.SetActive(false);
                _pool.Add(element);
            }

            foreach (var element in _freeElements)
            {
                element.gameObject.SetActive(false);
                _pool.Add(element);
            }

            _freeElements.Clear();
        }

    }
}